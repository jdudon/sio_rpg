# Cadre

BTS SIO

## SUJET

Programmation Orientée Objet


## TYPE

Mini projet

### Langage

PHP

### Desrciption

Le but est de créer le démarrage d'un mini jeu type RPG en PHP POO. 
Les apprenants doivent créer: 
- Un dossier RPG dans lequel on travaillera
- Un dossier class dans lequel on ajoutera chacune de nos classes
- Un fichier index.php qui sera notre fichier de base
L'objectif est d'avoir un formulaire qui demande un nom et de choisir entre 3 classes de personnage(Guerrier, Mage et Archer).
Chaque classe aura ses particularités concernant la manière d'attaquer (le guerrier frappe, le mage lance un sort et l'archer tire).
Une fois le personnage créée, le formulaire disparait et laisse apparaître 3 boutons. 2 sot communs à toutes les classes et le troisième est lié à la spécificité de la classe choisie par l'utilisateur (frapper/ lancer un sort/ tirer).
Chaque méthode crée affichera un simple echo. 

### Restrictions
- N'utiliser que du PHP et du HTML CSS (ce dernier est optionnel), Bootstrap est autorisé.
- Une classe principale doit être créée et des classes enfants doivent en hériter.

#### Version
Cette version-ci du projet est un corrigé. Il s'agit d'une manière de faire sans ajouter de notions non vues en cours pour le moment.
