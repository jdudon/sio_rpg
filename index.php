<?php
// Ne pas oublier d'appeler les classes utilisées
require "class/Mage.php";
require "class/Ranger.php";
require "class/Warrior.php";
// On vérifie que le formulaire POST lié au bouton nommé "submit" est bien envoyé
if (isset($_POST['submit'])) {
// Si c'est le cas, on démarre une session
    session_start();
// On vérifie quelles infos sont stockées dans la clé "rpg_class" de $_POST
    if ($_POST["rpg_class"] == "Warrior") {
        // On serialize (passe de type Objet à type String l'objet qu'on créer pour pouvoir le stocker dans ma variable $_session)
        $_SESSION['rpg_class'] = serialize(new Warrior($_POST['name']));
        // On stocke le type de classe choisie toujours dans la session pour pouvoir la réutiliser plus tard
        $_SESSION['class'] = 'Warrior';
    } elseif ($_POST["rpg_class"] == "Mage") {
        $_SESSION['rpg_class'] = serialize(new Mage($_POST['name']));
        $_SESSION['class'] = 'Mage';
    } elseif ($_POST["rpg_class"] == "Ranger") {
        $_SESSION['rpg_class'] = serialize(new Ranger($_POST['name']));
        $_SESSION['class'] = 'Ranger';
    } else {
    }
// On deserialize "rpg_class" (donc on le repasse de String à Objet)
    $newChar = unserialize($_SESSION['rpg_class']);
// Dès que le personnage est créée, il dit bonjour (c'est un ajout, une fantasie de ma part)
    $newChar->hello();
}
// Si le formulaire contentant le bouton nommé "reset" est envoyé je détruit la session (aussi un ajout de ma part. Enjoy ;))
if (isset($_POST['reset'])) {
    session_destroy();
}
// Même principe ici avec les boutons "move"n "speak", "punch", "spellcast", "shoot"
if (isset($_POST['move'])) {
    //Je récupère les données de la session
    session_start();
    //je deserialize l'objet dans "rpg_class"
    $newChar = unserialize($_SESSION['rpg_class']);
    //Je lance la méthode associée
    $newChar->move();
}
if (isset($_POST['speak'])) {
    session_start();
    $newChar = unserialize($_SESSION['rpg_class']);
    $newChar->speak();
}
if (isset($_POST['punch'])) {
    session_start();
    $newChar = unserialize($_SESSION['rpg_class']);
    $newChar->punch();
}
if (isset($_POST['spellcast'])) {
    session_start();
    $newChar = unserialize($_SESSION['rpg_class']);
    $newChar->spellcast();
}
if (isset($_POST['shoot'])) {
    session_start();
    $newChar = unserialize($_SESSION['rpg_class']);
    $newChar->shoot();
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- J'ajoute le lien qui permet d'appeler bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <title>RPG Basics</title>
</head>

<body>
    <!-- Si il existe une session -->
    <?php if ($_SESSION) : ?>
        <!-- Je créée une div qui contiendra mes boutons Avancer, Parler et Frapper/Lancer un sort/Tirer (en fonction de notre classe) -->
        <div>
            <h1>Cliquer pour effectuer une action</h1>
            <form action="" method="post">
                <div class="form-group">
                    <button class="btn btn-primary" type="submit" name="move">Avancer</button>
                    <button class="btn btn-primary" type="submit" name="speak">Parler</button>
                </div>
                <!-- Si la classe trouvée dans session est.... alors je créée un bouton avec le nom associé.... -->
                <?Php if ($_SESSION['class'] === 'Warrior') : ?>
                    <button class="btn btn-primary" type="submit" name="punch">Frapper </button>
                <?php elseif ($_SESSION['class'] === 'Mage') : ?>
                    <button class="btn btn-primary" type="submit" name="spellcast">Lancer un sort </button>
                <?php elseif ($_SESSION['class'] === 'Ranger') : ?>
                    <button class="btn btn-primary" type="submit" name="shoot">Tirer </button>
                <?php endif ?>

            </form>
        </div>
        <!-- La div contenant le formulaire de création de personnage ne doit pas apparaître -->
        <div style="display: none;">
        <!-- Si il n'y a pas de session en cours alors aucun personnage n'a été crée donc... -->
        <?php else : ?>
            <!-- J'ouvre une div sans le display none -->
            <div>
            <?php endif ?>
            <h1>Créer un nouveau perso</h1>
            <!-- Et je créée mon formulaire tout simplement -->
            <form action="" method="post">
                <div class="form-group">
                <label class="form-label" for="name">Nom: </label>
                <input class="form-control" type="text" name="name">
                </div>
                <div class="form-group">
                <label for="rpg_class">Classe: </label>
                <select class="form-control" name="rpg_class" id="rpg_class">
                    <option value="Warrior">Guerrier</option>
                    <option value="Mage">Mage</option>
                    <option value="Ranger">Archer</option>
                </select>
                </div>
                <button class="btn btn-success" type="submit" name="submit">Créer</button>
            </form>
            </div>
            <form action="" method="post">
                <button class="btn btn-danger" type="submit" name="reset">Reset</button>
            </form>
</body>

</html>