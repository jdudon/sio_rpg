<?php
// Je require_once plutôt que require tout court pour éviter que dans "index.php" je me retrouve à require plusieurs fois "Adventurer.php"
require_once "Adventurer.php";

class Mage extends Adventurer {
    public function spellCast() {
        echo "Je lance un sort";
    }
}