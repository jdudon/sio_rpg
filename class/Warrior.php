<?php
// Je require_once plutôt que require tout court pour éviter que dans "index.php" je me retrouve à require plusieurs fois "Adventurer.php"
require_once "Adventurer.php";

class Warrior extends Adventurer {
    public function punch() {
        echo "Je frappe";
    }
}