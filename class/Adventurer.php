<?php

class Adventurer {

    protected $_name;

    public function __construct(string $name)
    {
        $this->_name = $name;
    }
    public function hello(){
        echo "Hello mon nom est ".$this->_name." Ca va BG?";
    }
    public function move() {
        echo "J'avance";
    }
    public function speak() {
        echo "Je parle";
    }
}