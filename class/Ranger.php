<?php
// Je require_once plutôt que require tout court pour éviter que dans "index.php" je me retrouve à require plusieurs fois "Adventurer.php"
require_once "Adventurer.php";

class Ranger extends Adventurer {
    public function shoot() {
        echo "Je tire";
    }
}